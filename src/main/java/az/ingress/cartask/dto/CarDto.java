package az.ingress.cartask.dto;

import lombok.Data;

@Data
public class CarDto {

   private Long id ;
   private String name;
   private Long hp;

}
