package az.ingress.cartask.repository;

import az.ingress.cartask.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car,Long> {
}
