package az.ingress.cartask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartaskApplication.class, args);
	}

}
