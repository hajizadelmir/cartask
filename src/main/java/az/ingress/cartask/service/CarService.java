package az.ingress.cartask.service;

import az.ingress.cartask.dto.CarDto;

public interface CarService {
    CarDto getCarById(Long id);

    CarDto createCar(CarDto dto);

    void deleteCar(Long id);

    CarDto updateCar(CarDto dto);


}
