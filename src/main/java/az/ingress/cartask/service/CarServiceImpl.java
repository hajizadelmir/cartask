package az.ingress.cartask.service;

import az.ingress.cartask.dto.CarDto;
import az.ingress.cartask.model.Car;
import az.ingress.cartask.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository repository;
    private final ModelMapper mapper;
    @Override
    public CarDto getCarById(Long id) {
        Car byId = repository.getById(id);
        CarDto dto = mapper.map(byId, CarDto.class);
        return dto;

    }

    @Override
    public CarDto createCar(CarDto dto) {
        Car car = mapper.map(dto, Car.class);
        Car save = repository.save(car);
        return mapper.map(save,CarDto.class);



    }

    @Override
    public void deleteCar(Long id) {
        repository.deleteById(id);

    }

    @Override
    public CarDto updateCar(CarDto dto) {
        Car car = repository.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Car Not Found!!!")));
        car.setName(dto.getName());
        car.setHp(dto.getHp());
        Car save = repository.save(car);
        return mapper.map(save,CarDto.class);

    }
}
