package az.ingress.cartask.controller;

import az.ingress.cartask.dto.CarDto;
import az.ingress.cartask.service.CarServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarController {
    private final CarServiceImpl service;

    @GetMapping("/{id}")
    public CarDto getCarById (@PathVariable Long id){
        return service.getCarById(id);
    }
    @PostMapping
    public CarDto createCar (@RequestBody CarDto dto){
        return service.createCar(dto);
    }
    @DeleteMapping("/{id}")
    public void deleteCar(@PathVariable Long id){
         service.deleteCar(id);
    }
    @PutMapping
    public  CarDto updateCar(@RequestBody CarDto dto){
        return service.updateCar(dto);
    }
}
